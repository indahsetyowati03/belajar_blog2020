-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Sep 2020 pada 13.26
-- Versi server: 10.1.35-MariaDB
-- Versi PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_blog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `created_at`, `updated_at`, `deleted_at`, `category`) VALUES
(1, '7 Makanan Tradisional Khas Jawa yang Cocok Disajikan saat HUT RI ke-75', 'Jakarta - Makanan tradisional khas Jawa pilihannya sangat beragam. Semua makanan ini memiliki makna spesial sehingga jadi sajian populer saat perayaan-perayaan tertentu, termasuk pada Hari Kemerdekaan HUT RI yang ke-75 ini.', NULL, NULL, NULL, 'HUT RI ke-75'),
(2, 'test', 'test', NULL, NULL, NULL, 'test'),
(3, 'Dr.', 'Aut vel sit possimus ex quia enim perferendis. Sed voluptatem enim in magni libero dolores nobis.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Restaurant Cook'),
(4, 'Dr.', 'Perferendis nam perferendis porro. Est sapiente aut animi distinctio sint mollitia. Qui quo nisi excepturi doloremque. Consequatur officiis aspernatur consequatur.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Parking Enforcement Worker'),
(5, 'Miss', 'Eius a eligendi est iste pariatur dignissimos. Rerum rerum voluptatem quia rerum voluptates voluptatibus magnam amet. Eos odit nemo blanditiis deserunt occaecati aut maiores.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Welding Machine Setter'),
(6, 'Prof.', 'Tempore natus quisquam aut unde odio aut magni. Corporis at nisi ut voluptate est quidem. Itaque et aut earum assumenda aut dolor doloribus assumenda.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Naval Architects'),
(7, 'Mr.', 'Assumenda distinctio hic perferendis saepe et. Odio earum provident earum. Saepe qui praesentium enim.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Drycleaning Machine Operator'),
(8, 'Mrs.', 'Veniam aut quia ut ad minima. Ut eius nesciunt dolorum quam non. Ipsam impedit voluptatem consequatur consequatur alias. Occaecati praesentium sed consequatur.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Petroleum Engineer'),
(9, 'Dr.', 'Qui est quos rerum corporis. Sint quia sit eos nulla et quia. Porro similique aut aut rerum quidem omnis deleniti.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Product Management Leader'),
(10, 'Prof.', 'Non fugiat id sapiente nisi non tempora pariatur. Nihil ipsa repudiandae error culpa blanditiis nesciunt. Numquam architecto perspiciatis fugiat id veritatis totam nihil. Et aperiam et pariatur sed. Deserunt assumenda iusto unde deleniti.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Landscape Artist'),
(11, 'Mr.', 'Excepturi tempora voluptas corporis incidunt quo nihil corporis. Harum neque id voluptas blanditiis soluta quo culpa. Architecto itaque reiciendis culpa a sunt enim minima.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Geological Sample Test Technician'),
(12, 'Mr.', 'Debitis eaque nihil et at maxime suscipit accusantium. Consectetur recusandae qui deleniti fuga delectus. Occaecati sint rem dolore non quo neque.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Dental Laboratory Technician'),
(13, 'Ms.', 'Quo consectetur et totam est inventore iusto aut asperiores. Enim amet error qui aut ut. Quod sit porro et voluptas.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Postal Service Mail Sorter'),
(14, 'Dr.', 'Alias ut aut voluptatem tenetur nemo porro officiis. Aliquam fugit minus modi voluptatum alias delectus aut. Est repellendus quaerat tempora aperiam nihil iure consequatur. Voluptas iste doloribus pariatur quo id sint.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Title Examiner'),
(15, 'Mr.', 'Vel voluptatem omnis ad quo sed. Doloribus harum voluptatem esse consequatur repudiandae eos.', '2020-09-10 02:16:58', '2020-09-10 02:16:58', NULL, 'Home Economics Teacher'),
(16, 'Prof.', 'Unde neque autem in nam at culpa inventore et. Aut iure ea doloribus veritatis mollitia. Ex soluta non fugit. Reiciendis rem iusto aut quaerat tempore incidunt accusantium deserunt.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Natural Sciences Manager'),
(17, 'Dr.', 'Quo consequatur vitae perferendis enim vel. Culpa qui assumenda minima. Explicabo ducimus nisi nam nostrum. Laborum occaecati quasi aliquid necessitatibus magnam.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Heavy Equipment Mechanic'),
(18, 'Mr.', 'At temporibus omnis itaque quasi ad ea. Earum rerum in omnis possimus recusandae voluptatem.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Natural Sciences Manager'),
(19, 'Prof.', 'Blanditiis quia pariatur dolores totam. Cumque quod laboriosam qui ad consequatur laborum. Tempora in pariatur veritatis enim vitae omnis.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Food Batchmaker'),
(20, 'Miss', 'Eaque voluptas quae sapiente et veritatis quia molestias. Veniam fugiat totam nobis corrupti. Velit omnis inventore molestiae porro non.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Materials Engineer'),
(21, 'Ms.', 'Nihil natus id veritatis exercitationem minima. Provident dolorem sed laudantium ipsum. Animi vitae accusantium et.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Directory Assistance Operator'),
(22, 'Miss', 'Consequatur occaecati illum velit et omnis possimus sit soluta. Architecto doloribus sed ducimus et. Minima totam rerum a veritatis. Totam incidunt quos quasi illo.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Engine Assembler'),
(23, 'Mr.', 'Animi est laborum illo rem. Quod perspiciatis recusandae sint accusantium autem. Ad sed laborum et similique qui pariatur autem.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Spraying Machine Operator'),
(24, 'Ms.', 'Ut est mollitia nesciunt. Ut nostrum dolore itaque minima exercitationem aspernatur ex et. Perspiciatis reiciendis ut quia explicabo et optio error.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Petroleum Technician'),
(25, 'Prof.', 'Ipsam vitae cupiditate quam consequatur soluta. Voluptates sit ipsum ut totam beatae. Ex laborum voluptatem aut animi quas. Sequi est iusto id magnam.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Child Care Worker'),
(26, 'Prof.', 'Quaerat qui explicabo dolorem quae. Non magnam sunt vitae sed vitae autem consequatur. Ullam ea pariatur sed dolores illo nostrum.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Manager Tactical Operations'),
(27, 'Prof.', 'Non sint quos impedit adipisci omnis. Qui est et blanditiis veniam eum. Ut ducimus veniam eligendi nulla non.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Marketing Manager'),
(28, 'Mr.', 'Mollitia qui repellat totam assumenda quaerat dolor aspernatur. Aut ea alias aliquam et illo. Dicta hic quisquam eos dolorem.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'HVAC Mechanic'),
(29, 'Dr.', 'Debitis et cupiditate placeat aperiam autem dolore culpa id. Eaque eos sit magnam expedita consequatur. Aliquam quae ut quo aut fuga consequuntur laborum.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Floor Finisher'),
(30, 'Dr.', 'Molestias accusamus neque excepturi ut et. Ut autem nihil saepe facilis. Nihil impedit veniam sint minus occaecati assumenda vitae. Fuga ea officiis aliquam molestiae est sed excepturi.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Rehabilitation Counselor'),
(31, 'Dr.', 'Dicta doloribus quo rerum. Omnis aut consequuntur ex sit eos qui. Deleniti ducimus sint labore ad. Magnam nemo voluptas sapiente.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Child Care'),
(32, 'Mr.', 'Odit voluptatum pariatur qui officia aliquid dolores eaque. Perferendis earum voluptas iure laboriosam. Doloremque eveniet nisi aliquid dolor velit aut.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Audio-Visual Collections Specialist'),
(33, 'Prof.', 'Sit ducimus quo et. Totam ut esse a nostrum numquam adipisci distinctio. Illo ad dolore illum veniam sint. Ab est explicabo quia eveniet.', '2020-09-10 02:16:59', '2020-09-10 02:16:59', NULL, 'Annealing Machine Operator'),
(34, 'Mr.', 'Commodi magnam sint quo sit. Quam ut nemo tenetur quas quia ut voluptates. Labore voluptatem est occaecati ex voluptas quod laborum. Veritatis possimus aspernatur hic enim eligendi.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Chemical Technician'),
(35, 'Prof.', 'Eligendi et quisquam sint accusamus iste enim rerum. Reprehenderit voluptates perferendis sit ut quia facere. Adipisci eius natus vel exercitationem. Consequatur culpa ab occaecati qui ea odio id sequi. Rerum ducimus velit quod quae sapiente necessitatibus.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Biophysicist'),
(36, 'Mr.', 'Dicta nihil quo maiores. Suscipit nesciunt autem earum dolore dolore quod. Fugiat laborum iusto excepturi voluptate error odio neque.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Rental Clerk'),
(37, 'Mr.', 'Similique recusandae quaerat pariatur placeat vero dicta. Voluptas molestiae accusantium eum consequuntur hic odit. Similique aut aliquam quia ducimus velit. Unde nam magni omnis.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Self-Enrichment Education Teacher'),
(38, 'Prof.', 'Iusto voluptas quasi molestias vero. Dolorum vel qui est magnam voluptatem eos eum. Consequatur enim exercitationem ipsam et.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Marine Engineer'),
(39, 'Mrs.', 'Doloremque tempora commodi optio voluptatem beatae. Beatae doloremque omnis omnis pariatur blanditiis numquam nihil. Quisquam a ut totam pariatur et pariatur sequi.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Mathematician'),
(40, 'Dr.', 'Et voluptas aut velit atque temporibus quisquam. Qui est qui deleniti rerum ut eos. Et sit repudiandae similique deserunt officiis aut aliquid.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Soil Scientist'),
(41, 'Miss', 'Temporibus ea placeat quibusdam. Aut maxime dicta eum corporis possimus. Tempore cum repellat nemo consequatur.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Letterpress Setters Operator'),
(42, 'Miss', 'Debitis quo facilis suscipit quo quam rem accusamus. Rerum ducimus explicabo aut ea ratione quibusdam.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Community Service Manager'),
(43, 'Mr.', 'Et aspernatur repellendus cum. Ut dolorum quae voluptas a numquam. Inventore et tenetur quasi voluptatem rerum.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Psychiatric Aide'),
(44, 'Dr.', 'Beatae quod eveniet iure et. Eos est deleniti molestias. Dolorum vitae et nisi dignissimos.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Camera Repairer'),
(45, 'Mr.', 'Voluptatem qui a quo voluptate accusamus. Culpa debitis dolor repudiandae blanditiis atque ut ea. Qui voluptatum cumque et exercitationem. Voluptatibus nisi fuga corrupti suscipit minima nostrum omnis.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Database Administrator'),
(46, 'Mr.', 'Rerum illum qui quia qui quod. Et sint deleniti sunt pariatur magnam. Deleniti sequi accusamus voluptatem. Doloremque omnis ea similique.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Installation and Repair Technician'),
(47, 'Mr.', 'Quam quis inventore incidunt quia. Soluta qui saepe animi at assumenda libero. Tenetur illo voluptate tenetur omnis laboriosam nobis. Ad earum voluptatum et eligendi.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Screen Printing Machine Operator'),
(48, 'Mr.', 'Laboriosam eum iure est. Aut dolore delectus beatae et. Sint perferendis inventore ipsum pariatur sapiente unde. Itaque doloremque vel nisi.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Nonfarm Animal Caretaker'),
(49, 'Prof.', 'Ut sed ad libero at explicabo ratione. Dolor at mollitia neque repellendus quod ducimus voluptatum. Qui doloribus rerum dolore voluptatem voluptatem sequi. Modi libero dolorem cumque eaque unde.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Administrative Services Manager'),
(50, 'Dr.', 'Ipsum libero et doloremque dolorem quam odio ea illum. Doloremque est perferendis culpa numquam alias. Eos explicabo quaerat et consequuntur nostrum vel est. Assumenda fugit labore et id odio.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Medical Equipment Repairer'),
(51, 'Dr.', 'Ut voluptate consectetur ducimus in qui vitae dolorem. Aliquam eos placeat dolore maiores nihil dolorem. Natus sunt provident similique laborum necessitatibus ad. Aut quia nemo et sed odit provident dolores inventore.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Food Preparation'),
(52, 'Mrs.', 'Dolorem quia molestiae dolorem ducimus ea. Minus autem laudantium ut architecto nobis ex dolorum impedit. Id quidem assumenda necessitatibus molestiae.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Brazing Machine Operator'),
(53, 'Mr.', 'Quia numquam nesciunt suscipit facilis. Distinctio voluptas consequatur dolores fugit. Repellendus sint voluptatibus cum.', '2020-09-10 02:17:00', '2020-09-10 02:17:00', NULL, 'Supervisor Correctional Officer'),
(54, 'Ms.', 'Doloribus ut voluptatem voluptatem optio ut esse. Voluptatem fuga qui voluptas assumenda et assumenda voluptas. Est quos ea quae eum repellat voluptas fugiat ex. Possimus et consequatur doloribus autem quaerat et.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Environmental Science Technician'),
(55, 'Mrs.', 'Nobis explicabo quo enim placeat fugiat corrupti. Et quos dolorem aut sed. Quisquam aut quam sunt quas laborum cupiditate natus. Eveniet adipisci consequatur maiores et.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Order Filler'),
(56, 'Dr.', 'Numquam commodi dolorem fuga aut. Voluptas et quod ut ipsum. Occaecati sit numquam omnis quia. Et eos ipsa tenetur harum dolorem velit quos ut.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Heating and Air Conditioning Mechanic'),
(57, 'Mr.', 'Qui impedit aspernatur natus nisi. Dolore consequatur error provident eum occaecati enim. Aut commodi nostrum distinctio voluptatum illum qui consequatur. Accusamus rerum necessitatibus quia quam laudantium iusto.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Production Planning'),
(58, 'Prof.', 'Et sint molestias consequatur asperiores eos. Accusantium nihil rerum rerum enim et. Sapiente ipsum aut non.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Construction Driller'),
(59, 'Prof.', 'Omnis qui laborum quas aspernatur ut commodi. Qui voluptas possimus odit tenetur neque ipsa distinctio. Nihil veritatis ea beatae exercitationem eaque.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Food Batchmaker'),
(60, 'Miss', 'Itaque soluta quo fugiat qui perferendis. Ut commodi vel itaque culpa. Sit voluptas provident voluptatem eaque ut voluptate.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Roofer'),
(61, 'Ms.', 'Dolorem animi natus labore maxime. Iusto delectus sunt expedita sapiente odio ab aliquid. Ut voluptatem facere quibusdam ea aut.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Scientific Photographer'),
(62, 'Miss', 'Repudiandae saepe hic molestiae aut consequatur eveniet. Adipisci voluptatum maxime qui ad. Est sunt facilis sint dicta nemo. Dicta tenetur beatae nulla quia sapiente rerum. Distinctio est praesentium iure molestiae quisquam.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Computer Repairer'),
(63, 'Mr.', 'Unde nisi veniam architecto qui. Autem temporibus aliquam explicabo labore id eius. Est voluptates aliquam molestias dolores minima assumenda.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Woodworking Machine Setter'),
(64, 'Ms.', 'Voluptatum odit consequatur modi. Ut inventore non vero dolorem. Natus tempore quos qui. Et ad nobis ipsa nihil consequuntur molestiae dicta iure.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Recreational Therapist'),
(65, 'Mr.', 'Voluptas optio natus quibusdam occaecati id modi. Velit vel et ea sed molestiae. Laboriosam id delectus repellat autem tenetur unde temporibus maxime. Laborum temporibus consequatur mollitia qui sunt.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Inspector'),
(66, 'Dr.', 'Veritatis architecto culpa voluptas sint. Et totam qui exercitationem est aut et eveniet. Omnis doloremque molestiae sequi. Sed tempore provident repellendus vel eveniet deserunt.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Air Traffic Controller'),
(67, 'Miss', 'Modi et consequatur neque dolores deleniti suscipit totam. Quis sint deserunt unde ut. Qui velit consequatur doloribus voluptas ut eos. Iusto quaerat consectetur cupiditate dolores dignissimos debitis voluptate modi.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Education Teacher'),
(68, 'Dr.', 'Error ut libero quos vero magni velit asperiores. Numquam quis dolores nobis voluptatem quae sint voluptas optio. Eligendi adipisci accusantium maxime accusamus ducimus.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Insurance Policy Processing Clerk'),
(69, 'Prof.', 'Quis ratione sit sit necessitatibus. Eligendi quia dolores quia quae nam aut.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Biomedical Engineer'),
(70, 'Mrs.', 'Dolor aperiam nemo accusamus dignissimos. Modi ipsum enim rerum est adipisci quisquam. Ea a adipisci ut omnis et quam corporis. Nesciunt distinctio omnis blanditiis dolore velit est qui.', '2020-09-10 02:17:01', '2020-09-10 02:17:01', NULL, 'Nonfarm Animal Caretaker'),
(71, 'Dr.', 'Vero deleniti est quasi minus consequatur iusto dolorum. Velit rem ratione sint molestias consequatur veritatis.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Agricultural Worker'),
(72, 'Prof.', 'Qui soluta quis architecto. Nihil aut quis sit ea. Rem laborum id recusandae omnis qui.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Steel Worker'),
(73, 'Prof.', 'Atque corrupti non quam iure. Voluptas similique voluptatem dicta exercitationem nulla suscipit. Aut dolores cupiditate doloribus occaecati. Impedit ipsum aperiam excepturi repudiandae aut est.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Lay-Out Worker'),
(74, 'Mr.', 'Atque exercitationem et magnam quae quod repudiandae enim. Aut sed autem nostrum eligendi. Nihil perferendis fugit iste ratione accusantium quidem reiciendis in. Aliquid qui est ullam ea rerum qui eos.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Forestry Conservation Science Teacher'),
(75, 'Mr.', 'Maxime doloremque fuga itaque illum deleniti veritatis. Ab enim rerum quidem voluptatem. Et eos autem ipsam vel. Esse quam est similique voluptas. Totam non dolorem minus sed dicta necessitatibus minus.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Precision Instrument Repairer'),
(76, 'Miss', 'Consequatur sit et quas qui veniam placeat dolorum. Quam enim consequatur eos et. Nisi illum laudantium similique quas tenetur. Accusamus vitae consequatur reiciendis asperiores ea architecto.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Detective'),
(77, 'Prof.', 'Quia aut ut quam eligendi. Explicabo accusamus quas maxime voluptatum beatae. Placeat doloremque quia eos laborum. Nostrum id quo soluta ipsa eaque.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Metal Worker'),
(78, 'Dr.', 'Dolor rerum natus impedit. Consequuntur ullam nesciunt facilis. Nihil nam et quasi. Ad laboriosam id aut voluptas repellendus incidunt facere.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Transportation and Material-Moving'),
(79, 'Ms.', 'Eum sint dolorem similique dignissimos debitis voluptatem omnis doloribus. Excepturi ratione architecto qui sed expedita sed eum facere. Non totam id qui ut.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Agricultural Sales Representative'),
(80, 'Prof.', 'Qui facilis consequatur consectetur tempore quis facere. Provident labore totam dignissimos ut rerum neque ipsum possimus. Incidunt reprehenderit laboriosam neque excepturi cumque occaecati maiores.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Dentist'),
(81, 'Prof.', 'Quae sunt veritatis cumque sunt pariatur porro dolorem. Explicabo sit sint facilis hic omnis dolorum. Laborum accusantium eaque ut aut eos veritatis nihil. Aut explicabo mollitia nesciunt molestiae.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Forestry Conservation Science Teacher'),
(82, 'Mrs.', 'Perferendis et et omnis dolorem hic iure. Ipsa cumque dolorum reprehenderit repudiandae. Suscipit possimus ut nobis et doloribus rerum.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Stonemason'),
(83, 'Prof.', 'Iste fuga ea quae et ea occaecati molestiae. Dolores non quaerat molestiae. Fuga vel sint adipisci dolores.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Order Clerk'),
(84, 'Prof.', 'Iusto ut pariatur modi neque rerum aut sunt. Deleniti suscipit quod dolorum. Officia laboriosam ea eos dolores quia tenetur. Et rem dolores iure maxime aut omnis qui qui.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Drafter'),
(85, 'Prof.', 'Porro voluptas suscipit aut quas adipisci vero. Ut rerum possimus neque pariatur dolores veniam. Laborum blanditiis labore vero et.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Manager of Weapons Specialists'),
(86, 'Prof.', 'Commodi suscipit temporibus sapiente vel. Itaque perspiciatis et voluptatem consequatur. Eaque dolores nesciunt pariatur libero fugit reprehenderit.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Physician Assistant'),
(87, 'Prof.', 'Enim autem tempora ut voluptates nostrum sit. Recusandae rerum et iure quia aut libero officiis aperiam. Voluptatem sunt rerum dicta. Itaque ut non consequatur iste.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Alteration Tailor'),
(88, 'Dr.', 'Et officia enim deleniti minima libero sed sed recusandae. Est fugiat eligendi at voluptatem in perspiciatis ab. Est sed est velit fugiat.', '2020-09-10 02:17:02', '2020-09-10 02:17:02', NULL, 'Electrical and Electronic Inspector and Tester'),
(89, 'Dr.', 'Eum officia ipsa aut ab laborum aut. Qui dolorem est doloribus ea est aspernatur iusto iusto. Nemo magni ut rem aut harum.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Customer Service Representative'),
(90, 'Mr.', 'Quas adipisci earum omnis nihil illum autem tempore voluptatem. Sunt non ratione quae veniam. Voluptate veniam consequatur neque facilis saepe rem. Quae rerum consequatur molestiae ipsam dicta et ducimus.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Parking Lot Attendant'),
(91, 'Dr.', 'Vel sunt dolores ipsa veritatis temporibus. Et minus nemo omnis et. Fugit ipsa est qui esse tempore. Aperiam consectetur fugit harum qui in non velit.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Spraying Machine Operator'),
(92, 'Mr.', 'Iusto et consequatur ipsam et occaecati perferendis. Facilis est dicta voluptatem eum magnam voluptas. Sint optio reiciendis et recusandae maiores facere cupiditate. Porro tenetur quia expedita aut qui perferendis.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Biological Technician'),
(93, 'Prof.', 'Nostrum hic dicta rem saepe. Harum dolorem necessitatibus non iste non dignissimos officia.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Private Detective and Investigator'),
(94, 'Mrs.', 'Inventore asperiores non odit exercitationem unde veniam deserunt. Omnis aliquid voluptatem molestiae voluptas eum accusamus. Dolorum ratione exercitationem praesentium ea.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Agricultural Science Technician'),
(95, 'Dr.', 'Debitis laborum illum sed aliquam. Omnis repudiandae nostrum culpa molestias nostrum. Corporis modi eos iste quod laborum quo. Saepe ut illum est id dolores dolorem.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Home'),
(96, 'Prof.', 'Voluptatem dolorem voluptas rerum nisi. Aut quia sed ut architecto molestias molestiae rerum. Nisi consequuntur modi enim sit sint eius recusandae. Consequatur deserunt enim labore qui.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Homeland Security'),
(97, 'Dr.', 'Voluptas et quia esse eaque quidem et. Doloribus velit eos voluptatem asperiores harum. Sunt voluptate est quaerat quis libero corporis. Consequuntur ut qui sapiente maiores at harum enim.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Furniture Finisher'),
(98, 'Ms.', 'Laboriosam totam autem labore dolores incidunt et voluptatem aut. Quam nihil et consequatur quas architecto aperiam debitis. Quod ut sint ipsa dolorem natus similique sint.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Loan Interviewer'),
(99, 'Prof.', 'Nihil quidem reiciendis molestiae enim. Non fugit nostrum et aut odio expedita. Quia natus sunt sapiente. Corporis fugit recusandae sit qui ut iste. Odio cum tempore laudantium.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Segmental Paver'),
(100, 'Prof.', 'Et consequatur odit harum. Similique dicta quam voluptas praesentium ex magnam. Omnis debitis in laboriosam quasi magni in. Omnis facilis minus illum perspiciatis qui consectetur qui.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Numerical Tool Programmer OR Process Control Programmer'),
(101, 'Mr.', 'Soluta vel dolor deserunt minus saepe saepe. Aliquam totam ratione est ratione nostrum natus tenetur.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'HVAC Mechanic'),
(102, 'Ms.', 'Deserunt et rerum pariatur ab autem minima eum neque. Sequi dolores earum quo numquam. Corporis id ut iusto quisquam necessitatibus consectetur.', '2020-09-10 02:17:03', '2020-09-10 02:17:03', NULL, 'Sculptor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(6, '2014_10_12_000000_create_users_table', 1),
(7, '2014_10_12_100000_create_password_resets_table', 1),
(8, '2019_08_19_000000_create_failed_jobs_table', 1),
(9, '2020_09_10_072154_create_blog_table', 1),
(10, '2020_09_10_074646_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
